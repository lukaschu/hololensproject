#pragma once
#include "HL2ResearchMode.g.h"

// WARNING: This file is automatically generated by a tool. Do not directly
// add this file to your project, as any changes you make will be lost.
// This file is a stub you can use as a starting point for your implementation.
//
// To add a copy of this file to your project:
//   1. Copy this file from its original location to the location where you store 
//      your other source files (e.g. the project root). 
//   2. Add the copied file to your project. In Visual Studio, you can use 
//      Project -> Add Existing Item.
//   3. Delete this comment and the 'static_assert' (below) from the copied file.
//      Do not modify the original file.
//
// To update an existing file in your project:
//   1. Copy the relevant changes from this file and merge them into the copy 
//      you made previously.
//    
// This assertion helps prevent accidental modification of generated files.
static_assert(false, "This file is generated by a tool and will be overwritten. Open this error and view the comment for assistance.");

namespace winrt::HL2UnityPlugin::implementation
{
    struct HL2ResearchMode : HL2ResearchModeT<HL2ResearchMode>
    {
        HL2ResearchMode() = default;

        com_array<float> GetLUTEntries(array_view<float const> uv);
        uint16_t GetCenterDepth();
        com_array<uint16_t> GetDepthMapBuffer();
        com_array<uint8_t> GetDepthMapTextureBuffer(int64_t& ts);
        com_array<uint16_t> GetShortAbImageBuffer();
        com_array<uint8_t> GetShortAbImageTextureBuffer();
        com_array<uint16_t> GetLongAbImageBuffer();
        com_array<uint8_t> GetLongAbImageTextureBuffer();
        com_array<float> GetPointCloudBuffer();
        com_array<uint16_t> GetLongDepthMapBuffer();
        com_array<uint8_t> GetLongDepthMapTextureBuffer();
        com_array<float> GetLongThrowPointCloudBuffer();
        com_array<uint8_t> GetLFCameraBuffer(int64_t& ts);
        com_array<uint8_t> GetRFCameraBuffer(int64_t& ts);
        com_array<uint8_t> GetLRFCameraBuffer(int64_t& ts_left, int64_t& ts_right);
        com_array<float> GetAccelSample();
        com_array<float> GetGyroSample();
        com_array<float> GetMagSample();
        com_array<float> GetCenterPoint();
        int32_t GetDepthBufferSize();
        int32_t GetLongDepthBufferSize();
        hstring PrintDepthResolution();
        hstring PrintDepthExtrinsics();
        hstring PrintLFResolution();
        hstring PrintLFExtrinsics();
        hstring PrintRFResolution();
        hstring PrintRFExtrinsics();
        bool DepthMapTextureUpdated();
        bool ShortAbImageTextureUpdated();
        bool LongAbImageTextureUpdated();
        bool PointCloudUpdated();
        bool LongDepthMapTextureUpdated();
        bool LongThrowPointCloudUpdated();
        bool LFImageUpdated();
        bool RFImageUpdated();
        bool AccelSampleUpdated();
        bool GyroSampleUpdated();
        bool MagSampleUpdated();
        void InitializeDepthSensor();
        void InitializeLongDepthSensor();
        void InitializeSpatialCamerasFront();
        void InitializeAccelSensor();
        void InitializeGyroSensor();
        void InitializeMagSensor();
        void StartDepthSensorLoop();
        void StartDepthSensorLoop(bool reconstructPointCloud);
        void StartLongDepthSensorLoop();
        void StartLongDepthSensorLoop(bool reconstructPointCloud);
        void StartSpatialCamerasFrontLoop();
        void StartAccelSensorLoop();
        void StartGyroSensorLoop();
        void StartMagSensorLoop();
        void StopAllSensorDevice();
        void SetReferenceCoordinateSystem(winrt::Windows::Perception::Spatial::SpatialCoordinateSystem const& refCoord);
        void SetPointCloudRoiInSpace(float centerX, float centerY, float centerZ, float boundX, float boundY, float boundZ);
        void SetPointCloudDepthOffset(uint16_t offset);
    };
}
namespace winrt::HL2UnityPlugin::factory_implementation
{
    struct HL2ResearchMode : HL2ResearchModeT<HL2ResearchMode, implementation::HL2ResearchMode>
    {
    };
}
